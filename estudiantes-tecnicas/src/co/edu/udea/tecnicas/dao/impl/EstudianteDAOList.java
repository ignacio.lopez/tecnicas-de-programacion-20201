package co.edu.udea.tecnicas.dao.impl;

import co.edu.udea.tecnicas.dao.EstudianteDAO;
import co.edu.udea.tecnicas.model.Estudiante;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EstudianteDAOList implements EstudianteDAO {

    private static List<Estudiante> estudiantesDB = new ArrayList<>();

    @Override
    public void registrarEstudiante(Estudiante estudiante) {
        estudiantesDB.add(estudiante);
    }

    @Override
    public Optional<Estudiante> consultarPorId(String id) {
        for(Estudiante estudiante:estudiantesDB) {
           if(estudiante.getId().equals(id)){
              return Optional.of(estudiante);
           }
       }
       return Optional.empty();
    }
}
