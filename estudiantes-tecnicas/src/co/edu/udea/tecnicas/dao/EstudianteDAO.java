package co.edu.udea.tecnicas.dao;

import co.edu.udea.tecnicas.model.Estudiante;

import java.util.Optional;

public interface EstudianteDAO {

    void registrarEstudiante(Estudiante estudiante);

    Optional<Estudiante> consultarPorId(String id);
}
