package co.edu.udea.tecnicas.bsn;

import co.edu.udea.tecnicas.bsn.exception.EstudianteYaExisteException;
import co.edu.udea.tecnicas.dao.EstudianteDAO;
import co.edu.udea.tecnicas.dao.impl.EstudianteDAOList;
import co.edu.udea.tecnicas.model.Estudiante;

import java.util.Optional;

public class EstudianteBsn {

    private EstudianteDAO estudianteDAO;

    public EstudianteBsn() {
        this.estudianteDAO = new EstudianteDAOList();
    }


    public void registrarEstudiante(Estudiante estudiante) throws EstudianteYaExisteException {
        Optional<Estudiante> estudianteOptional = this.estudianteDAO.consultarPorId(estudiante.getId());
        // el estudiante no estaba
        if (estudianteOptional.isPresent()) {
            throw new EstudianteYaExisteException();
        } else {
            this.estudianteDAO.registrarEstudiante(estudiante);
        }
    }
}
