package co.edu.udea.tecnicas.bsn.exception;

public class EstudianteYaExisteException extends Exception {

    public EstudianteYaExisteException() {
        super("Ya existe un estudiante con el id ingresado");
    }
}
