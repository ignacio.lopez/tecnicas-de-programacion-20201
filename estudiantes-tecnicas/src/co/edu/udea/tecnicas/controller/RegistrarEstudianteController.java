package co.edu.udea.tecnicas.controller;

import co.edu.udea.tecnicas.bsn.EstudianteBsn;
import co.edu.udea.tecnicas.bsn.exception.EstudianteYaExisteException;
import co.edu.udea.tecnicas.model.Estudiante;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class RegistrarEstudianteController {

    // Binding
    @FXML
    private TextField txtIdentificacion;
    @FXML
    private TextField txtNombres;
    @FXML
    private TextField txtApellidos;
    @FXML
    private TextField txtCarrera;

    // conexión con el negocio
    private EstudianteBsn estudianteBsn;

    public RegistrarEstudianteController() {
        this.estudianteBsn = new EstudianteBsn();

    }

    public void btnGuardar_action() {
        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String idIngresado = txtIdentificacion.getText().trim();
        String nombresIngresados = txtNombres.getText().trim();
        String apellidosIngresados = txtApellidos.getText().trim();
        String carreraIngresada = txtCarrera.getText().trim();

        // se valida que el id contenga un valor
        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", "El id es requerido");
            txtIdentificacion.requestFocus();
            return;
        }
        // se valida que el id sea un número
        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtIdentificacion.requestFocus();
            txtIdentificacion.clear();
            return;
        }

        // se valida que los nombres contengan un valor
        if (nombresIngresados.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", "Los nombres son requeridos");
            txtNombres.requestFocus();
            return;
        }

        // se valida que los apellidos contengan un valor
        if (apellidosIngresados.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", "Los apellidos son requeridos");
            txtApellidos.requestFocus();
            return;
        }

        // se valida que la carrera contenga un valor
        if (carreraIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", "La carrera es requerida");
            txtCarrera.requestFocus();
            return;
        }

        Estudiante estudiante = new Estudiante(idIngresado, nombresIngresados, apellidosIngresados, carreraIngresada);
        try {
            //todo bien
            this.estudianteBsn.registrarEstudiante(estudiante);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de estudiante", "Resultado de la transacción", "El estudiante ha sido registrado con éxito");
            limpiarCampos();
        } catch (EstudianteYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de estudiante", "Resultado de la transacción", e.getMessage());
        }
    }

    // método para validar un conjunto de Strings: null o vacío
    private boolean validarCampos(String... campos) {
        boolean sonValidos = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                sonValidos = false;
                break;
            }
        }
        return sonValidos;
    }

    private void limpiarCampos() {
        this.txtIdentificacion.clear();
        this.txtNombres.clear();
        this.txtApellidos.clear();
        this.txtCarrera.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
