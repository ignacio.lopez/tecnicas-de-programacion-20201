import javax.swing.*;

public class Mayor100Numeros {

    public static void main(String[] args) {
        //variables
        int numero, cont, mayor;

        //inicio
        cont = 0;
        mayor = 0;
        while (cont < 10) {
            numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número:"));
            if (numero > mayor) {
                mayor = numero;
            }
            cont++;
        }
        JOptionPane.showMessageDialog(null, "El mayor es: "+mayor);
    }
}
