import java.util.Scanner;

public class Suma2Numeros {

    public static void main(String[] args) {
        // variables
        int numero1, numero2, suma;
        Scanner lector = new Scanner(System.in); //objeto para leer datos desde consola

        //inicio
        System.out.println("Ingrese el primer número:");
        numero1 = lector.nextInt();
        System.out.println("Ingrese el segundo número:");
        numero2 = lector.nextInt();
        suma = numero1 + numero2;
        System.out.println("La suma es: " + suma);
        System.out.println(String.format("La suma es: %s", suma));

    }

}
