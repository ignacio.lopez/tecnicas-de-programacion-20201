package co.edu.udea.tecnicas.s20201.model;

import java.util.LinkedList;
import java.util.List;

public class Estudiante {
    // Atributos de la clase
    protected String nombres;
    private String id;
    private char genero;

    // Atributos que representan las relaciones con otras clases
    private List<Nota> notas;

    // constructores

    /**
     * @param nombres
     * @param id
     * @param genero  mínimo debe existir uno para cada clase. Si no se crea explícitamente, el compilador crea uno por defecto: sin parámetros ni comportamiento.
     *                Se puede usar para obligar al usuario (clase) a entregar ciertos parámetros para la creación del objeto.
     *                Se puede para inicializar las variables de la clase
     */
    public Estudiante(String nombres, String id, char genero) {
        this.nombres = nombres;
        this.id = id;
        this.genero = genero;

        // se crea una lista vacía de notas
        notas = new LinkedList();
    }

    public Estudiante() {
        this.nombres = "NN";
        this.id = "000";
        this.genero = 'n';

        // se crea una lista vacía de notas
        notas = new LinkedList();
    }

    // métodos principales de estudiante

    public void agregarNota(Nota nota) {
        this.notas.add(nota);
    }

    public double calcularPromedio() {
        if(notas.isEmpty()){
            System.out.println("No hay notas por procesar");
            return 0.0;
        }
        double suma = 0.0;
        for (Nota nota : notas) {
            suma += nota.getValor();
        }
        double promedio = suma / notas.size();
        return promedio;
    }


    public void presentarse() {
        String presentacion = "Hola, mi nombre es " + nombres + " y mi id es " + id;
        System.out.println(presentacion);
    }


    // Getters && Setters
    public void setNombres(String nombres) {
        // O de corto circuito: Si la primera expresión es falsa, no evalúa la segunda
        if (nombres == null || nombres.equals("Hitler")) {
            System.out.println("na ah");
            return;
        }
        this.nombres = nombres;
        System.out.println("Ahora me llamo: " + nombres);
    }

    public String getNombres() {
        return nombres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }
}
