package co.edu.udea.tecnicas.s20201.model;

public class Nota {
    // Atributos de la clase
    private int numero;
    private String nombreCurso;
    private double valor;
    private double porcentaje;



    public Nota(int numero, String nombreCurso, double valor, double porcentaje) {
        this.numero = numero;
        this.nombreCurso = nombreCurso;
        this.valor = valor;
        this.porcentaje = porcentaje;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

}
