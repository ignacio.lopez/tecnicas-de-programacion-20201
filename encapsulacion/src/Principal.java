import co.edu.udea.tecnicas.s20201.model.Estudiante;
import co.edu.udea.tecnicas.s20201.model.Nota;


class Principal {

    public static void main(String[] args) {
        Estudiante e1 = new Estudiante("AAAA", "111", 'M');
        e1.setNombres(null);
        e1.presentarse();

        Nota nota1= new Nota(1, "Técnicas de programación", 3.1, 0.06);

        Nota nota2= new Nota(2, "Técnicas de programación", 4.2, 0.06);


        e1.agregarNota(nota1);
        e1.agregarNota(nota2);

        System.out.println(e1.calcularPromedio());

        //co.edu.udea.tecnicas.s20201.model.Estudiante e2 = new co.edu.udea.tecnicas.s20201.model.Estudiante();
        // e2.presentarse();
    }
}
