public class Principal {

    public static void main(String[] args) {
        /*Persona p = new Persona("111", "persona 1");
        System.out.println(p.presentarse());
        System.out.println(p);*/


        Persona e = new Estudiante("222", "estudiante 1");
        System.out.println(e.presentarse());
        e.caminar();

        Estudiante eConvertido = (Estudiante) e; // top-down casting
        eConvertido.getPromedio();

        Object o = e; //bottom-up casting
        o.toString();

        // Profesor eConvertidoAProfesor = (Profesor) e; error de ClassCastException

        Persona profesor = new Profesor();
        System.out.println(profesor.presentarse());
        profesor.caminar();
    }
}
