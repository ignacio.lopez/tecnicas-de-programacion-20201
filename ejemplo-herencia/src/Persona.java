public abstract class Persona {
    protected String id;
    protected String nombres;

    public Persona(String id, String nombres) {
        this.id = id;
        this.nombres = nombres;
    }

    // método abstracto
    public abstract void caminar();

    // método concreto
    public String presentarse() {
        return "Hola, me llamo " + nombres;
    }

    //
    @Override
    public String toString() {
        return id + " - " + nombres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
}
