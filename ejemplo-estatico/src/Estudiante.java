public class Estudiante {

    private String id;
    private String nombres;
    private static int contadorEstudiantes;

    public Estudiante(String id, String nombres) {
        this.id = id;
        this.nombres = nombres;
        contadorEstudiantes++;
    }

    public static int getContadorEstudiantes() {
        return contadorEstudiantes;
    }

    public static void describirEstudiante(){
        System.out.println("Soy un estudiante que no entrego la práctica");
    }

}
