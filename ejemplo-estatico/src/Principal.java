public class Principal {

    public static void main(String[] args) {
        System.out.println(Math.PI);

        System.out.println(Estudiante.getContadorEstudiantes());

        Estudiante estudiante1 = new Estudiante("123", "Joan");
        System.out.println(Estudiante.getContadorEstudiantes());

        Estudiante estudiante2 = new Estudiante("456", "Juan");
        System.out.println(Estudiante.getContadorEstudiantes());

        Estudiante.describirEstudiante();

    }
}
