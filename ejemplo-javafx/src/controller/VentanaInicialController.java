package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

import java.util.Random;

public class VentanaInicialController {

    @FXML
    private HBox hboxFichas;
    @FXML
    private Button btnLimpiar;

    public void btnLimpiar_action() {
        // this.hboxFichas.setVisible(false);
        this.hboxFichas.getChildren().clear();
        btnLimpiar.setVisible(false);
    }

    public void btnAgregar_clic(ActionEvent actionEvent) {
        System.out.println("Entra a agregar un hijo");
        int numero = new Random().nextInt(7);
        Button botonNuevo = new Button(String.valueOf(numero));
        botonNuevo.setOnAction(this::btnCualquiera_action);

        this.hboxFichas.getChildren().add(0, botonNuevo);
        this.btnLimpiar.setVisible(true);
    }

    public void btnCualquiera_action(ActionEvent actionEvent){
        System.out.println("me hicieron clic");
        Button botonInvocador = (Button) actionEvent.getSource();
        System.out.println(botonInvocador);
        System.out.println(botonInvocador.getText());
    }


}
