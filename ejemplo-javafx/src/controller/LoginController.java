package controller;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {

    @FXML
    private Label lblNombres;
    @FXML
    private TextField txtNombres;
    @FXML
    private TextField txtApellidos;

    private Stage stage;

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public void cmdIngresar_action() throws IOException {
        System.out.println("hicieron clic en el botón ingresar");
        System.out.println("ingresó los nombres: "+ txtNombres.getText());
        System.out.println("Ingresó los apellidos: "+txtApellidos.getText());
        lblNombres.setText("Se hizo clic en el botón");


        Parent ventanaInicial = FXMLLoader.load(getClass().getResource("../view/ventana-inicial.fxml"));

        stage.setTitle("Ventana inicial");
        stage.setScene(new Scene(ventanaInicial, 600, 600));
    }
}
