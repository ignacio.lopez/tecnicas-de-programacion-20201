public class Paloma extends Animal implements Volador {
    @Override
    public void hacerRuido() {
        System.out.println("coo coo!");
    }

    @Override
    public void moverse() {
        System.out.println("Avanzo en dos patas");
    }

    @Override
    public void volar() {
        System.out.println("Vuelo como paloma");
    }
}
