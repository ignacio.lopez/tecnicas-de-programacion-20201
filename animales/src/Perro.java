public class Perro extends Animal{
    private String raza;

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public void hacerRuido() {
        System.out.println("bark bark!");
    }

    @Override
    public void moverse() {
        System.out.println("Avanzo en cuatro patas");
    }
}
