public class Principal {

    public static void main(String[] args) {
        Persona pAnonima = new Persona();
        pAnonima.identificacion = "123";
        pAnonima.nombres = "Juan";
        pAnonima.edad = 22;

        Persona pNombres = new Persona("Jesús");

        Persona pIdNombres = new Persona("123", "María");

    }
}
