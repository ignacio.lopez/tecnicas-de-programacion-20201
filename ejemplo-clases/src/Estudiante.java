/**
 * tipo de dato personalizado que representa a
 * un estudiante
 * PascalCase
 */
public class Estudiante {
    //atributos
    //camelCase
    String carrera;
    String numeroDocumento;
    byte semestre;
    short creditosAprobados;
    String nombres;
    String apellidos;
    byte edad;

    @Override
    public boolean equals(Object that) {
        if (this == that) return true; // se evalúa si las dos variables apuntan al mismo espacio en memoria
        Estudiante otro = (Estudiante) that; // se hace la conversión del Object llamado that a Estudiante llamado otro
        return semestre == otro.semestre &&
                creditosAprobados == otro.creditosAprobados &&
                edad == otro.edad &&
                carrera.equals(otro.carrera) &&
                numeroDocumento.equals(otro.numeroDocumento) &&
                nombres.equals(otro.nombres) &&
                apellidos.equals(otro.apellidos);
    }

    @Override
    public String toString() {
        return numeroDocumento + " - " + nombres + " " + apellidos;
    }


}
