import javax.swing.*;

public class Principal3 {

    static Estudiante[] estudiantes = new Estudiante[3];

    public static void main(String[] args) {
        String menu = "Bienvenido: \n" +
                "1. Registrar estudiante.\n" +
                "2. Listar estudiantes.\n" +
                "3. Salir.";

        String opcion = "";
        int contadorEstudiantes = 0;
        do {
            opcion = JOptionPane.showInputDialog(menu);
            switch (opcion) {
                case "1":
                    // todo validar cantidad de estudiantes ingresados
                    // permite registrar un estudiante
                    Estudiante estudianteNuevo = new Estudiante();
                    String numeroDocumento = JOptionPane.showInputDialog("Ingrese el número de documento:");
                    estudianteNuevo.numeroDocumento = numeroDocumento;
                    String nombres = JOptionPane.showInputDialog("Ingrese los nombres:");
                    estudianteNuevo.nombres = nombres;
                    String apellidos = JOptionPane.showInputDialog("Ingrese los apellidos:");
                    estudianteNuevo.apellidos = apellidos;

                    //todo completar el ingreso del resto de los datos

                    estudiantes[contadorEstudiantes] = estudianteNuevo;
                    contadorEstudiantes++;
                    break;
                case "2":
                    // salir de la aplicación
                    for(Estudiante estudiante: estudiantes){
                        System.out.println(estudiante);
                    }
                    break;
            }

        } while (!"3".equals(opcion)); //programación a la defensiva para evitar NullPointerException
    }
}
