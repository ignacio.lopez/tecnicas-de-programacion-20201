public class Principal {

    public static void main(String[] args) {
        int a = 5;
        Integer aObjeto =5;
        System.out.println(a);
        int b = a;
        System.out.println(b);
        b = 8;
        System.out.println(a);

        // creamos la instancia de Estudiante llamada e1
        Estudiante e1 = new Estudiante();
        // le asignamos valores a los atributos del estudiante recién creado
        e1.carrera = "Ingeniería de Sistemas";
        e1.semestre = 2;
        e1.numeroDocumento = "123";
        e1.nombres = "Aaaa";
        e1.apellidos = "Aaaaaaaa";
        e1.creditosAprobados = 23;
        e1.edad = 20;

        System.out.println(e1.edad);

        Estudiante e2 = new Estudiante();
        e2.carrera = "Ingeniería de Sistemas";
        e2.semestre = 2;
        e2.numeroDocumento = "123";
        e2.nombres = "Aaaa";
        e2.apellidos = "Aaaaaaaa";
        e2.creditosAprobados = 23;
        e2.edad = 20;

        System.out.println(e1.equals(e2));
    }
}
