import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Mapas {

    public static void main(String[] args) {
        Map<String, Ciudadano> mapaCiudadanos = new HashMap<>();

        mapaCiudadanos.put("123", new Ciudadano("123", "Ciudadano 1", "Ingeniero de dominoes"));

        Ciudadano ciudadano2 = new Ciudadano("456", "Ciudadano 2", "Técnico en dominoes");

        mapaCiudadanos.put(ciudadano2.getId(), ciudadano2);

        Ciudadano ciudadanoExtraido = mapaCiudadanos.get("789");
        System.out.println(ciudadanoExtraido);

        ciudadanoExtraido = mapaCiudadanos.get("123");
        System.out.println(ciudadanoExtraido);

        // método para evaluar si una llave hace parte del mapa o no
        if (mapaCiudadanos.containsKey("789")) {
            System.out.println("Contiene el dato con clave 789");
        } else {
            System.out.println("No contiene el dato con clave 789");
        }

        // método para recorrer las llaves del mapa
        Set<String> llavesMapa = mapaCiudadanos.keySet();
        for(String llave: llavesMapa){
            System.out.println(llave);
        }

        // método para recorrer los objetos (Valores) del mapa
        Collection<Ciudadano> valoresMapa = mapaCiudadanos.values();
        for(Ciudadano ciudadano: valoresMapa){
            System.out.println(ciudadano);
        }

        // método para recorrer las entradas del mapa
        Set<Map.Entry<String, Ciudadano>> entradasMapa = mapaCiudadanos.entrySet();
        for(Map.Entry entrada: entradasMapa){
            System.out.println(entrada.getKey() + " -- "+entrada.getValue());
        }
    }
}
