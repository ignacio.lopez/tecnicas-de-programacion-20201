public class Ciudadano {

    private String id;
    private String nombres;
    private String profesion;

    public Ciudadano(String id, String nombres, String profesion) {
        this.id = id;
        this.nombres = nombres;
        this.profesion = profesion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    @Override
    public String toString() {
        return "Ciudadano{" +
                "id='" + id + '\'' +
                ", nombres='" + nombres + '\'' +
                ", profesion='" + profesion + '\'' +
                '}';
    }
}
