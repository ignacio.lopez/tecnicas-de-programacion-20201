import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {

    public static void main(String[] args) {
        Collection<String> coleccion = new ArrayList<>();
        coleccion.add("one");
        coleccion.add("two");
        coleccion.add("trois");
        coleccion.add("cinco");
        coleccion.add("sechs");
        coleccion.add("sieben");
        coleccion.add("noventa");



        coleccion.stream().forEach(Streams::imprimir);

        System.out.println("-------------SÓLO AQUELLOS QUE CONTENGAN DOS Ns----------------");
        Stream<String> numerosFiltrados = coleccion.stream().filter(Streams::verificarNN);
        numerosFiltrados.forEach(Streams::imprimir);

        System.out.println("------------CONVERTIR EN MAYÚSCULAS-----------------");
        Stream<String> numerosMayuscula = coleccion.stream().map(numero -> numero.toUpperCase());
        numerosMayuscula.forEach(Streams::imprimir);

        System.out.println("------------RESULTADO COMO LISTA-----------------");
        List<String> numerosFiltradosLista = coleccion.stream().filter(t -> t.startsWith("t")).collect(Collectors.toList());
        numerosFiltradosLista.forEach(Streams::imprimir);

        System.out.println("-------------EL QUE CONTENGA MENOR CANTIDAD DE CARACTERES----------------");
        String shortest = coleccion.stream().min(Comparator.comparing(item -> item.length())).get();
        System.out.println(shortest);

        System.out.println("---------------LA CANTIDAD DE ELEMENTOS QUE CONTENGAN UNA N --------------");
        long count = coleccion.stream().filter(item -> item.contains("n")).count();
        System.out.println(count);

        System.out.println("--------------UN STRING CON TODOS LOS ELEMENTOS DEL STREAM SEPARADOS POR ESPACIO---------------");
        String reduced = coleccion.stream().reduce((acc, item) -> acc + " " + item).get();
        System.out.println(reduced);
    }

    public static void imprimir(String texto) {
        System.out.println(texto);
    }

    public static boolean verificarNN(String texto) {
        int contadorN = 0;
        for (int i = 0; i < texto.length(); i++) {
            if (texto.charAt(i) == 'n' || texto.charAt(i) == 'N') {
                contadorN++;
            }
        }
        return contadorN >= 2;
    }
}
