public class PromedioArreglo {

    static int[] arreglo = {2, 3, 6, 7, 8, 9};

    public static void main(String[] args) {
        // variables
        double promedio;
        int suma, cont;

        //inicio
        suma = 0;
        for (cont = 0; cont < arreglo.length; cont++) {
            suma += arreglo[cont];
        }
        promedio = (double) suma / arreglo.length;
        System.out.println("El promedio de los datos es: " + promedio);
    }
}
