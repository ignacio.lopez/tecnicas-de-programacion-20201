import javax.swing.*;
import java.math.BigInteger;

public class Menu {

    public static void main(String[] args) {
        String menu = "Bienvenido al reto 2\n" +
                "Seleccione una opción:\n" +
                "1. Hacer algo 1.\n" +
                "2. Hacer otra cosa 2.\n" +
                "3. Salir.";
        String opcion = "";

        do {
            opcion = JOptionPane.showInputDialog(menu);
            switch (opcion) {
                case "1":
                    System.out.println("seleccionó la opción 1");
                    break;
                case "2":
                    System.out.println("Seleccionó la opción 2");
                    break;
                case "3":
                    System.out.println("Suerte!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opción incorrecta ");
            }
        } while (!opcion.equals("3"));


    }
}
